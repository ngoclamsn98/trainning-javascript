/*
    ngoài việc khác nhau về cú pháp thì for và forEach còn khác nhau:
    for:      
              +  có thể dùng được break hoặc continute
              +  cho phép truy cập vào index của mảng chứ không phải là phần tử thực tế, thế nên cần sử dụng arr[i] để lấy giá trị
              +  không bỏ các phần tử rỗng khi duyệt mảng
              +  Scope của this bên trong for chính là scope bên ngoài của các cấu trúc lặp này
              +  hoạt động tốt với Async/Await hoặc Generators
    forEach:  + không thể dùng được break hoặc continute
              + có thể truy cập trực tiếp đến giá trị của phần tử và truy cập đến index của phần tử
              + bỏ qua các phần tử rỗng khi duyệt mảng
              + Scope của this bên trong forEach() thì không như vậy trừ khi dùng arrow function
              + không hoạt động với Async/Await hoặc Generators(bởi vì forEach không chờ asynchronous process trong callback. Điều đó có nghĩa là, forEach không xây dựng trong quá trình asynchronous.)

*/


/*forEach: thì không dùng được break và continue*/
function run() {
    const arr = ['a', 'b', 'c'];
    arr.forEach(el => {
        //continue; // or break;
    });
}

/*for: thì dùng được break và continue*/
function run2() {
    const arr = ['a', 'b', 'c'];
    for (let index = 0; index < arr.length; index++) {
        break; // or continute;
    }
}

/* forEach: có thể truy cập trực tiếp đến giá trị của phần tử và truy cập đến index của phần tử*/
function run3() {
    const arr = ['a', 'b', 'c'];
    arr.forEach((v, index) => console.log(v, index));
}

/*for: cho phép truy cập vào index của mảng chứ không phải là phần tử thực tế, thế nên cần sử dụng arr[i] để lấy giá trị*/
function run4() {
    for (let i = 0; i < arr.length; ++i) {
        console.log(arr[i]);
    }
}



/*forEach: bỏ qua các phần tử rỗng khi duyệt mảng còn for thường thì không*/
function run5() {
    const arr = ['a', , 'c'];
    arr.forEach(v => console.log(v));
}

function run6() {
    const arr = ['a', , 'c'];
    for (let i = 0; i < arr.length; ++i) {
        console.log(arr[i]);
    }
}



/*Scope của this bên trong for chính là scope bên ngoài của các cấu trúc lặp này, forEach() thì không như vậy trừ khi dùng arrow function*/


function run7() {
    const arr = ['a', 'b', 'c'];
    for (let i = 0; i < arr.length; ++i) {
        console.log(this);
    }
}

function run8() {
    const arr = ['a', 'b', 'c'];
    arr.forEach(function (v) {
        console.log(this)
    });
}

async function run9() {
    const arr = ['a', 'b', 'c'];
    arr.forEach(async el => {
        await new Promise(resolve => setTimeout(resolve, 1000));
        console.log(el);
    });
}

function* run10() {
    const arr = ['a', 'b', 'c'];
    arr.forEach(el => {
        yield new Promise(resolve => setTimeout(resolve, 1000));
        console.log(el);
    });
}