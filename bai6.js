/*các phương pháp copy
    + có thể sử dụng spread operator
    + object.assign
    + JSON.parse và JSON.stringify
*/


/*
    khác nhau giữa shallow copy và deep copy
    + Shallow copying nhiệm vụ của nó chỉ copy những giá trị nông nghĩa là nó chỉ sao chép các giá trị đối tượng bình thường nhưng các giá trị lồng nhau vẫn sử dụng reference đến một đối tượng ban đầu.
    + Deep copy cũng giống như clone shallow nhưng các giá trị reference trong object gốc không thay trong object clone.
*/

/*
    có phải trường hợp nào cũng sử dụng deepcopy không vì sao ?
    + cũng tùy phụ thuộc vào bài toán : nếu trường hợp muốn thay đổi các property lồng nhau mà không ảnh hường đến object ban đầu thì sử dụng deep copy, còn các object nông thì chỉ cần shallow copy
    + với mỗi lần dùng deep copy thì vùng nhớ được tạo ra thêm ở thời điểm đó , sẽ gây tốn bộ nhớ khi không cần thiết
*/