async function main(a) {
    const array = Array.from(Array(1000000).keys());
    const promises = new Array(array.length).fill(Promise.resolve("ok"));
    const value = await Promise.all(promises);
    console.log(value, 'log');
}


main('1');
main('2');
main('3');