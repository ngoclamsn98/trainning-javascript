/*
- setImmediate : Hàm callback trong setImmediate() sẽ được thực thi trong vòng lặp tiếp theo
- process.nextTick: cách để thực hiện lời gọi hàm callback ngay lập tức
- Lỗi Zalgo khi lập trình Node.js: Lỗi Zalgo xảy ra khi trộn lẫn synchronous call back với asynchronous call back trong control flow : if then else hoặc loop. Việc này khiến cho ứng dụng Node.js cực kỳ khó dự đoán thứ tự thực thi code
*/