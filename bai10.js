
/*
test1: thừa await, 1 hàm có async trước function thì luôn trả về 1 promise
test2: thừa await, 1 hàm có async trước function thì luôn trả về 1 promise
test3: thừa await, 1 hàm có async trước function thì luôn trả về 1 promise
test4: đúng, nếu trường hợp lỗi thì hàm waitAndMaybeReject đã throw ra lỗi rồi nên không cần throw nữa, còn trường hợp không có lỗi thì sẽ return về kết quả cho function 
*/