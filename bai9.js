let p1 = new Promise(resolve => resolve("result1"));
let p2 = new Promise((resolve, reject) => reject('reject value'));
let p3 = new Promise(resolve => resolve("result3"));

const testFunc = async () => {
    const value = await Promise.all([p1, p2, p3].map(p => {
        return p.then(result => {
            return { status: 'fulfilled', value: result }
        }).catch(error => {
            return { status: "rejected", value: error }
        });

    }));
    console.log(value, 'what sup men');
}

testFunc();