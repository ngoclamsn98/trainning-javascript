/*chia ra */

let counter = 0;

let interval;

const listOfDelays = [];

for (let i = 0; i < 20; i++) {
    listOfDelays.push(Math.ceil(Math.random() * 10) * 1000);
}

const listOfArguments = Array.from(Array(20).keys());


/*asyncOperation: một hàm giả lập một hành động bất đồng bộ: trả về một Promise - và sẽ được resolved.*/
const asyncOperation = index => {
    counter++;
    return new Promise(resolve =>
        setTimeout(() => {
            console.log('vi tri:', index);
            console.log('time delay', listOfDelays[index]);
            counter--;
            resolve(index);
        }, listOfDelays[index]))
};


/*Hàm in ra số Promise được thực thi mỗi giây (để theo dõi)*/
const watchCounter = () => {
    console.log('Promises running in the beginning:', counter);

    if (interval) {
        clearInterval(interval);
    }

    interval = setInterval(() => console.log('Promises đang chạy:', counter), 1000);
};

watchCounter()

/**/
async function take3subtake1part1() {
    const concurrencyLimit = 5;
    const argsCopy = [].concat(listOfArguments.map((val, ind) => ({ val, ind })));
    const result = new Array(listOfArguments.length);
    const promises = new Array(concurrencyLimit).fill(Promise.resolve());
    function chainNext(p) {
        if (argsCopy.length) {
            const arg = argsCopy.shift();
            return p.then(() => {
                const operationPromise = asyncOperation(arg.val).then(r => { result[arg.ind] = r; });
                return chainNext(operationPromise);
            });
        }
        return p;
    }
    await Promise.all(promises.map(chainNext));
    return result;
}
take3subtake1part1().then(res => {
    console.log('result', res);
});