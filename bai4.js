/*
thời gian nhận kết quả request đầu tiên của controller1 là 10s còn của controller2 là 30s
*/


const waitBlocking = (miliSecond) => {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + miliSecond);
}

const waitNonBlocking = (miliSecond) => {
    return new Promise(resolve => setTimeout(() => resolve()), miliSecond);
}

const controller1 = async (req, res) => {
    await waitBlocking(10000);
}

const controller2 = (req, res) => {
    waitBlocking(10000);
}

/*cải thiện controller2*/

class SimpleQueue {
    constructor() {
        this.tasks = [];
        this.isRunning = false;
    }

    add(task) {
        this.tasks.push(task);
        if (!this.isRunning) {
            this._run();
        }
    }

    _run() {
        this.countRun += 1;
        this.isRunning = true;
        while (this.tasks.length) {
            const currentTask = this.tasks.shift();
            this.isRunning = false;
            console.log(currentTask);
        }
    }
}
const eventQueue = new SimpleQueue();

const controller2A = () => {
    eventQueue.add(waitBlocking(100))
}

controller2A();
controller2A();
controller2A();