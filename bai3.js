/*
thời gian để nhận được phản hồi của request đầu tiên là 31s
thời gian để nhận được phản hồi của request cuối cùng là 33s
thời gian trung bình để nhận được phản hồi của 3 request là 32s

*/

/*làm thế nào để request đầu tiên trả về kết quả trong khoảng 11s:  đẩy function doA vào event queue */

const controller = async (req, res) => {
    process.nextTick(() => {
        doA();
    })
    await doB();
    res.status(200).end();
}